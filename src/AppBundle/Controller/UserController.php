<?php
namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class UserController extends Controller
{

    /**
     * @Route("/users/profile/{id}", name="users_profile")
     * @Template
     */
    public function profileAction(Request $request, User $user)
    {
        $this->denyAccessUnlessGranted('edit', $user);
        $options['hide_roles'] =  $this->getUser() === $user;
        $form = $this->createForm(UserType::class, $user, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('fos_user.user_manager')->updateCanonicalFields($user);
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->addFlash('success', $this->get('translator')->trans('users.profile_updated'));
            return $this->redirectToRoute('admin_index');
        }
        return ['form' => $form->createView(), 'user' => $user];
    }

    /**
     * @Route("/users/view/{id}", name="users_view")
     * @Template
     */
    public function viewAction(User $user)
    {
        $this->denyAccessUnlessGranted('view', $user);
        $issues = $this->getDoctrine()->getRepository('AppBundle:Issue')->getOpenedIssuesByAssignee($user);
        return ['user' => $user, 'issues' => $issues];
    }

    /**
     * @Route("/users", name="users_index")
     * @Template
     */
    public function indexAction()
    {
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();
        return ['users' => $users];
    }
}
