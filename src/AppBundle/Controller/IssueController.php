<?php
namespace AppBundle\Controller;

use AppBundle\Form\IssueType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Project;
use AppBundle\Entity\Issue;
use Doctrine\Common\Util\Inflector;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class IssueController extends Controller
{

    const CODE_DELIMITER = '-';

    /**
     * @Route("/issues", name="issues_index")
     *
     * @Template
     */
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:Issue');
        //Check for role admin or manager
        $checker = $this->get('security.authorization_checker');
        if ($checker->isGranted('ROLE_ADMIN') || $checker->isGranted('ROLE_MANAGER')) {
            $issues = $repo->findAll();
        } //Operator see only assigned projects issues
        else {
            $user = $this->getUser();
            $projects = $user->getProjects()->getValues();
            $issues = $repo->getIssuesByProjects($projects);
        }

        return ['issues' => $issues];
    }

    /**
     * @Route("projects/{id}/create_issue/{parentIssue}", defaults={"parentIssue=null"}, name="issues_create")
     * @ParamConverter("project", class="AppBundle:Project")
     * @ParamConverter("parentIssue", class="AppBundle:Issue", options={"id" = "parentIssue"})
     *
     * @Template
     */
    public function createAction(Request $request, Project $project, Issue $parentIssue = null)
    {
        $this->denyAccessUnlessGranted('view', $project);
        $issue = new Issue();
        $issue->addProject($project);

        if ($parentIssue && $parentIssue->getType() == Issue::STORY_TYPE) {
            $issue->setParent($parentIssue);
            $issue->setReporter($parentIssue->getReporter());
            $issue->setType(Issue::SUBTASK_TYPE);
        } elseif ($parentIssue && $parentIssue->getType() != Issue::STORY_TYPE) {
            throw $this->createAccessDeniedException('You cannot access this page!');
        } else {
            $options['types'] = $this->getIssueTypes();
            $issue->setReporter($this->getUser());
        }

        $options['users'] = $project->getUsers()->getValues();
        $form = $this->createForm(IssueType::class, $issue, $options);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($issue);
            $em->flush();
            //Create code with id
            $code = ucfirst(Inflector::camelize($issue->getSummary())) . self::CODE_DELIMITER . $issue->getId();
            $issue->setCode($code);
            $em->flush();
            $this->addFlash('success', $this->get('translator')->trans('issues.created_success_msg'));
            return $this->redirectToRoute('issues_index');
        }

        return ['form' => $form->createView(), 'project' => $project, 'parent_issue' => $parentIssue];
    }

    /**
     * @Route("issues/edit/{id}",   name="issues_edit")
     * @ParamConverter("issue", class="AppBundle:Issue")
     *
     * @Template
     */
    public function editAction(Request $request, Issue $issue)
    {
        $project = $issue->getProject();
        $this->denyAccessUnlessGranted('view', $project);
        $options['users'] = $project->getUsers()->getValues();
        $options['types'] = $this->getIssueTypes();
        $form = $this->createForm(IssueType::class, $issue, $options);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($issue->getStatus() == 'closed') {
                $issue->setWasClosed(true);
            }

            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->addFlash('success', $this->get('translator')->trans('issues.updated_success_msg'));
            return $this->redirectToRoute('issues_index');
        }

        return ['form' => $form->createView(), 'project' => $project, 'issue' => $issue];
    }

    /**
     * @Route("issues/view/{id}",   name="issues_view")
     * @ParamConverter("issue", class="AppBundle:Issue")
     * @Template
     */
    public function viewAction(Issue $issue)
    {
        $project = $issue->getProject();
        $this->denyAccessUnlessGranted('view', $project);
        $childIssues = $parentIssue = [];

        if ($issue->getType() == Issue::STORY_TYPE) {
            $option = ['parentId' => $issue->getId()];
            $childIssues = $this->getDoctrine()->getRepository('AppBundle:Issue')->findBy($option);
        } elseif ($issue->getType() == Issue::SUBTASK_TYPE && $issue->getParentId()) {
            $option = ['id' => $issue->getParentId()];
            $parentIssue = $this->getDoctrine()->getRepository('AppBundle:Issue')->findOneBy($option);
        }

        return ['issue' => $issue, 'child_issues' => $childIssues, 'parent_issue' => $parentIssue];
    }

    /**
     * Issue types for select
     * @return array
     */
    private function getIssueTypes()
    {
        $types = [
            'issues.task_type' => Issue::TASK_TYPE,
            'issues.bug_type' => Issue::BUG_TYPE,
            'issues.story_type' => Issue::STORY_TYPE,
        ];

        return $types;

    }

    private function masterChanges()
    {
        return 'three ops';
    }
}
