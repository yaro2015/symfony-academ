<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="admin_index")
     * @Template
     */
    public function indexAction(Request $request)
    {
        $issues = $this->getDoctrine()
            ->getRepository('AppBundle:Issue')
            ->getOpenedIssuesByCollaborator($this->getUser());
        return ['issues' => $issues];
    }
}
