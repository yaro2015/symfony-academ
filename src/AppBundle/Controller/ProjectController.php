<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Project;
use AppBundle\Form\ProjectType;
use Doctrine\Common\Util\Inflector;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class ProjectController extends Controller
{

    const CODE_DELIMITER = '-';

    /**
     * @Route("/projects", name="projects_index")
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Project');

        //Check for role admin or manager
        $checker = $this->get('security.authorization_checker');
        if ($checker->isGranted('ROLE_ADMIN') || $checker->isGranted('ROLE_MANAGER')) {
            $projects = $repository->findAll();
        } //Operator see only assigned projects
        else {
            $user = $this->getUser();
            $projects = $user->getProjects();
        }


        return $this->render(
            'AppBundle:Project:index.html.twig',
            array('projects' => $projects)
        );
    }

    /**
     * @Route("/projects/create", name="projects_create")
     */
    public function createAction(Request $request)
    {
        $project = new Project();
        $form = $this->createForm(ProjectType::class, $project);
        //Set current user checked
        $form['users']->setData([$this->getUser()]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->flush();

            //Create code with id
            $code = ucfirst(Inflector::camelize($project->getTitle())) . self::CODE_DELIMITER . $project->getId();
            $project->setCode($code);

            $em->flush();
            $this->addFlash('success', $this->get('translator')->trans('projects.created_success_msg'));
            return $this->redirectToRoute('projects_index');
        }

        return $this->render(
            'AppBundle:Project:create.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/projects/view/{id}", name="projects_view")
     * @ParamConverter("project", class="AppBundle:Project")
     *
     */
    public function viewAction(Project $project)
    {
        $this->denyAccessUnlessGranted('view', $project);
        return $this->render(
            'AppBundle:Project:view.html.twig',
            ['project' => $project]
        );
    }

    /**
     * @Route("/foo/{id}", name="foo")
     * @param $id
     * @return Response
     */
    public function foo($id)
    {
        return new Response($id);
    }

    /**
     * @Route("/projects/edit/{id}", name="projects_edit")
     * @ParamConverter("project", class="AppBundle:Project")
     */
    public function editAction(Project $project, Request $request)
    {
        $this->denyAccessUnlessGranted('edit', $project);
        $form = $this->createForm(ProjectType::class, $project);
        //Set current user checked
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //Add users_projects relation
            $em->flush();
            $this->addFlash('success', $this->get('translator')->trans('projects.edited_success_msg'));
            return $this->redirectToRoute('projects_index');
        }

        return $this->render(
            'AppBundle:Project:edit.html.twig',
            array('form' => $form->createView())
        );
    }
}
