<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Form\CommentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Issue;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class CommentController extends Controller
{

    /**
     * @Route("/issues/{id}/create_comment", name="comments_create")
     * @Template
     */
    public function createAction(Request $request, Issue $issue)
    {
        $this->denyAccessUnlessGranted('view', $issue->getProject());
        $comment = new Comment();

        $options['action'] = $this->generateUrl(
            'comments_create',
            ['id' => $issue->getId()]
        );
        $form = $this->createForm(CommentType::class, $comment, $options);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $comment->setUser($this->getUser());
            $comment->setIssue($issue);
            $em->persist($comment);
            $em->flush();
            $this->addFlash('success', $this->get('translator')->trans('comments.added'));
            return $this->redirectToRoute('issues_view', ['id' => $issue->getId()]);
        }

        return ['form' => $form->createView()];
    }

    /**
     * @Route("/issues/edit_comment/{id}", name="comments_edit")
     * @Template
     */
    public function editAction(Request $request, Comment $comment)
    {
        $this->denyAccessUnlessGranted('MODIFY', $comment);
        $issue = $comment->getIssue();
        $this->denyAccessUnlessGranted('view', $issue->getProject());

        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
            $this->addFlash('success', $this->get('translator')->trans('comments.updated'));
            return $this->redirectToRoute('issues_view', ['id' => $issue->getId()]);
        }

        return ['form' => $form->createView()];
    }
    /**
     * @Route("/issues/delete_comment/{id}", name="comments_delete")
     *
     */
    public function deleteAction(Comment $comment)
    {
        $this->denyAccessUnlessGranted('MODIFY', $comment);
        $issue = $comment->getIssue();
        $this->denyAccessUnlessGranted('view', $issue->getProject());
        $em = $this->getDoctrine()->getManager();
        $em->remove($comment);
        $em->flush();
        $this->addFlash('success', $this->get('translator')->trans('comments.deleted'));
        return $this->redirectToRoute('issues_view', ['id' => $issue->getId()]);
    }
}
