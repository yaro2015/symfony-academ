<?php
namespace AppBundle\Twig;

use AppBundle\Entity\Activity;
use AppBundle\Repository\ActivityRepository;

class ActivityExtension extends \Twig_Extension
{
    private $activityRepository;

    public function __construct(ActivityRepository $repo)
    {
        $this->activityRepository = $repo;
    }

    public function getName()
    {
        return 'activity_extension';
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction(
                'show_activities',
                [$this, 'getActivities'],
                ['is_safe' => ['html'], 'needs_environment' => true]
            ),

        ];
    }

    /**
     * show_activities available params
     * @return array
     */
    public function getAvailableParams()
    {
        return [
            'issue' => 'getIssueActivities',
            'user' => 'getUserActivities',
            'project' => 'getProjectActivities',
            'user_projects' => 'getUserProjectsActivities'

        ];
    }

    /**
     * @param \Twig_Environment $environment
     * @param $type
     * @param $obj
     * @return string
     */
    public function getActivities(\Twig_Environment $environment, $type, $obj)
    {
        $statuses = [
            'issue_changed_status' => ' changed status of issue ',
            'issue_comment' => ' commented issue ',
            'issue_created' => ' created issue '
        ];
        $activities = [];
        $params = $this->getAvailableParams();
        if (isset($params[$type])) {
            $activities = $this->activityRepository->$params[$type]($obj);
        } else {
            return 'You passed wrong parameter';
        }

        return $environment->loadTemplate("AppBundle:Twig:activities.html.twig")->render(
            [
                'activities' => $activities,
                'statuses' => $statuses
            ]
        );
    }
}
