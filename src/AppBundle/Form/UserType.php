<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $timezones = \DateTimeZone::listIdentifiers();
        $timezones =  array_combine($timezones, $timezones);
        $builder
            ->add('username', TextType::class, [
             
            ])
            ->add('fullname', TextType::class, [
                'required' => false

            ])
            ->add('email', EmailType::class, [

            ])
            ->add('timezone', ChoiceType::class, [
                'choices' => $timezones,
                'choices_as_values' => true
                //'data' => 'Europe/Kiev'
            ]);

        if (!$options['hide_roles']) {
            $builder->add('roles', ChoiceType::class, [
                'choices' => [
                    'Admin' => 'ROLE_ADMIN',
                    'Manager' => 'ROLE_MANAGER',
                    'Operator' => 'ROLE_OPERATOR',
                ],
                'choices_as_values' => true
            ]);
            $builder->get('roles')
                ->addModelTransformer(new CallbackTransformer(
                    function ($roles) {
                        // transform the roles object  to a string
                        return  $roles[0];
                    },
                    function ($role) {
                        // transform the string back to an array
                        return [$role];
                    }
                ));
        }

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'hide_roles' => false
        ));
    }
}
