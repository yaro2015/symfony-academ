<?php
namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IssueType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('summary')
            ->add('description');


        $builder->add('resolution', ChoiceType::class, [
            'choices' => [
                'Fixed' => 'fixed',
                'Done' => 'done',
                'Incomplete' => 'incomplete',
                'Rejected' => 'rejected'
            ],
            'placeholder' => 'Choose if resolved',
            'required' => false,
            'choices_as_values' => true
        ]);


        $builder->add('priority', ChoiceType::class, [
            'choices' => [
                'Low' => 'low',
                'Middle' => 'middle',
                'Hight' => 'hight'

            ],
            'choices_as_values' => true
        ])
            ->add('status', ChoiceType::class, [
                'choices' => [
                    'Open' => 'open',
                    'In progress' => 'in_progress',
                    'Closed' => 'closed'
                ],
                'choices_as_values' => true
            ]);


        $builder->add('assignee', EntityType::class, [
            'choices' => $options['users'],
            'class' => 'AppBundle\Entity\User',
            'choices_as_values' => true

        ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'preSetData']);
    }

    /**
     * PRE_SET_DATA event handler
     *
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();
        $issue = $event->getData();
        $options = $form->getConfig()->getOptions();

        //Show select of types  and reporters if not parent issue (story) defined
        //AND
        //This  issue has not children assigned (if it is STORY without children it is OK)
        if (!$issue->getParent() && !$issue->getChildren()->count()) {
            $form->add('reporter', EntityType::class, [
                'choices' => $options['users'],
                'class' => 'AppBundle\Entity\User'

            ]);
            $form->add('type', ChoiceType::class, [
                'choices' => $options['types'],
                'choices_as_values' => true
            ]);
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Issue',
            'users' => [],
            'types' => []
        ));
    }
}
