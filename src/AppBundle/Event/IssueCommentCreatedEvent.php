<?php
namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\Comment;

/**
 * The issue.comment_added event
 */
class IssueCommentCreatedEvent extends Event
{
    const NAME = 'issue.comment_created';

    /**
     * @var Comment
     */
    protected $comment;

    /**
     * IssueCommentCreatedEvent constructor.
     * @param Comment $comment
     */
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return Comment
     */
    public function getComment()
    {
        return $this->comment;
    }
}
