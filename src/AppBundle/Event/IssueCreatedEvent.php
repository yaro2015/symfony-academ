<?php
namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\issue;

/**
 * The issue.issue_added event
 */
class IssueCreatedEvent extends Event
{
    const NAME = 'issue.created_event';

    /**
     * @var issue
     */
    protected $issue;

    /**
     * IssueCreatedEvent constructor.
     * @param issue $issue
     */
    public function __construct(Issue $issue)
    {
        $this->issue = $issue;
    }

    /**
     * @return issue
     */
    public function getIssue()
    {
        return $this->issue;
    }
}
