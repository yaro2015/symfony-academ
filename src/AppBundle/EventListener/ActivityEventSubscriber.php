<?php
namespace AppBundle\EventListener;

use AppBundle\Entity\Activity;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ActivityEventSubscriber implements EventSubscriberInterface
{
    const ISSUE_CREATED = 'issue_created';
    const ISSUE_UPDATED = 'issue_updated';
    const ISSUE_COMMENT = 'issue_comment';
    const ISSUE_CHANGED_STATUS = 'issue_changed_status';

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * IssueCollaboratorEventSubscriber constructor.
     * @param EntityManager $em
     */
    public function __construct(
        EntityManager $em,
        TokenStorageInterface $tokenStorage,
        \Swift_Mailer $mailer,
        TwigEngine $twig
    ) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'issue.comment_created' => 'addActivityComment',
            'issue.created_event' => 'addActivityIssue',
            'issue.status_changed_event' => 'addActivityIssueStatus',
        ];
    }

    /**
     * @param $event
     */
    public function addActivityComment($event)
    {
        $comment = $event->getComment();
        $issue = $comment->getIssue();
        $this->commonActions($issue, self::ISSUE_COMMENT);
    }

    /**
     * @param $event
     */
    public function addActivityIssue($event)
    {

        $issue = $event->getIssue();
        $this->commonActions($issue, self::ISSUE_CREATED);
    }

    /**
     * @param $event
     */
    public function addActivityIssueStatus($event)
    {
        $issue = $event->getIssue();
        $this->commonActions($issue, self::ISSUE_CHANGED_STATUS);
    }

    /**
     * @param $issue
     * @param $status
     */
    public function commonActions($issue, $status)
    {

        $token =  $this->tokenStorage->getToken();

        if ($token) {
            $activity = new Activity();
            $user = $token->getUser();
            $activity->setUser($user);
            $activity->setIssue($issue);
            $activity->setStatus($status);
            $this->em->persist($activity);
            $this->em->flush();
            $this->sendActivityEmail($issue->getCollaborators(), $activity);
        }

    }


    /**
     * @param $collaborators
     * @param $activity
     * @throws \Twig_Error
     */
    public function sendActivityEmail($collaborators, $activity)
    {
        $to = [];
        foreach ($collaborators as $c) {
            $to[] = $c->getEmail();
        }
        $template = $this->twig->render(
            'AppBundle:Emails:activity.html.twig',
            ['activity' => $activity]
        );
        $message = \Swift_Message::newInstance()
            ->setSubject('You have new activity')
            ->setFrom('send@example.com')
            ->setTo($to)
            ->setBody(
                $template,
                'text/html'
            );
        $this->mailer->send($message);
    }
}
