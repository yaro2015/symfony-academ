<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20.05.16
 * Time: 19:35
 */

namespace AppBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RegistrationListener implements EventSubscriberInterface
{

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess'

        ];
    }

    /**
     * @param FormEvent $event
     */
    public function onRegistrationSuccess(FormEvent $event)
    {
        $roles = ['ROLE_OPERATOR'];
        $user = $event->getForm()->getData();
        $user->setRoles($roles);
        $user->setTimezone('Europe/Kiev');

    }
}
