<?php
namespace AppBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class IssueCollaboratorEventSubscriber implements EventSubscriberInterface
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * IssueCollaboratorEventSubscriber constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'issue.comment_created' => 'addCommentCollaborator',
            'issue.created_event'   => 'addIssueCollaborators',
            'issue.updated_event'   => 'addIssueCollaborators',
        ];
    }

    /**
     * @param $event
     */
    public function addCommentCollaborator($event)
    {
        $comment = $event->getComment();
        $issue = $comment->getIssue();
        $issue->addCollaborator($comment->getUser());
        $this->em->persist($issue);
        $this->em->flush();

    }
    /**
     * Common collabolators adding stuff
     * @param $event
     *
     */
    public function addIssueCollaborators($event)
    {
        $issue = $event->getIssue();
        $issue->addCollaborator($issue->getAssignee());
        $issue->addCollaborator($issue->getReporter());
        $this->em->persist($issue);
        $this->em->flush();
    }
}
