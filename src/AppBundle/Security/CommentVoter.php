<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 24.05.16
 * Time: 13:44
 */

namespace AppBundle\Security;

use AppBundle\Entity\Comment;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;

class CommentVoter extends Voter
{
    const MODIFY = 'MODIFY';
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    public function supports($attribute, $subject)
    {

        // if the attribute isn't one we support, return false
        if ($attribute !== self::MODIFY) {
            return false;
        }

        // only vote on Project objects inside this voter
        if (!$subject instanceof Comment) {
            return false;
        }

        return true;
    }

    public function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        $comment = $subject;

        if ($this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return true;
        } else {
            return $comment->getUser() === $user;
        }
    }
}
