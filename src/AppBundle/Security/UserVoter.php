<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 24.05.16
 * Time: 13:44
 */

namespace AppBundle\Security;

use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;

class UserVoter extends Voter
{
    private $decisionManager;

    const VIEW = 'view';
    const EDIT = 'edit';

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    public function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::VIEW, self::EDIT))) {
            return false;
        }
        // only vote on Project objects inside this voter
        if (!$subject instanceof User) {
            return false;
        }

        return true;
    }

    public function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $subject;
        if ($attribute == self::VIEW) {
            return true;
        } elseif ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        } else {
            return  $user === $token->getUser();
        }
    }
}
