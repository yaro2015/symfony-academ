<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 24.05.16
 * Time: 13:44
 */

namespace AppBundle\Security;

use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;

class ProjectVoter extends Voter
{
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    public function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, ['view', 'edit'])) {
            return false;
        }

        // only vote on Project objects inside this voter
        if (!$subject instanceof Project) {
            return false;
        }

        return true;
    }

    public function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        $project = $subject;

        if ($this->decisionManager->decide($token, ['ROLE_ADMIN', 'ROLE_MANAGER'])) {
            return true;
        } elseif ($attribute == 'edit') {
            return false;
        } else {
            return $project->getUsers()->contains($user);
        }
    }
}
