<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Activity;
use AppBundle\Entity\Issue;
use AppBundle\Entity\Project;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;

class LoadFixtures implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();
        $userAdmin->setUsername('admin');
        $userAdmin->setEmail('admin@email.com');
        $userAdmin->setPassword('$2y$13$599z7t13c8co0o84sgocsOhyl6bUEZROIFvH5ieib7GKuKkQS5tOS');
        $userAdmin->setRoles(['ROLE_ADMIN']);

        $manager->persist($userAdmin);
        $manager->flush();

        $project = new Project();
        $project->setTitle('Title');
        $project->setSummary('Summary');
        $project->setCode('some-project-code');
        $manager->persist($project);
        $manager->flush();

        $issue = new Issue();
        $issue->setReporter($userAdmin);
        $issue->setAssignee($userAdmin);
        $issue->setSummary('sssss');
        $issue->setType('story');
        $issue->setPriority('low');
        $issue->setStatus('open');
        $issue->setProject($project);
        $issue->addCollaborator($userAdmin);
        $manager->persist($issue);
        $manager->flush();

        $activity = new Activity();

        $activity->setUser($userAdmin);
        $activity->setIssue($issue);
        $activity->setStatus('issue_created');
        $manager->persist($activity);
        $manager->flush();




    }
}
