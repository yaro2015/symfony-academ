<?php
namespace Tests\AppBundle\Entity;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class IssueRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var User
     */
    private $user;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->user = $this->em->getRepository('AppBundle:User')->find(1);
    }

    public function testGetIssuesByProjects()
    {
        $project = $this->em->getRepository('AppBundle:Project')->find(1);
        $issues = $this->em
            ->getRepository('AppBundle:Issue')
            ->getIssuesByProjects([$project]);


        $this->assertTrue(!empty($issues));
    }

    public function testGetOpenedIssuesByCollaborator()
    {
        $issues = $this->em
            ->getRepository('AppBundle:Issue')
            ->getOpenedIssuesByCollaborator($this->user);

        $this->assertTrue(!empty($issues));
    }
    
    public function testGetOpenedIssuesByAssignee()
    {
        $issues = $this->em
            ->getRepository('AppBundle:Issue')
            ->getOpenedIssuesByAssignee($this->user);

        $this->assertTrue(!empty($issues));
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }
}
