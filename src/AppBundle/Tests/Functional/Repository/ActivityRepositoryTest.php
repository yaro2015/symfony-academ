<?php
namespace Tests\AppBundle\Entity;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ActivityRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var User
     */
    private $user;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->user = $this->em->getRepository('AppBundle:User')->find(1);
    }

    public function testGetIssueActivities()
    {
        $issue = $this->em->getRepository('AppBundle:Issue')->find(1);
        $issues = $this->em
            ->getRepository('AppBundle:Activity')
            ->getIssueActivities($issue);


        $this->assertTrue(!empty($issues));
    }

    public function testGetUserActivities()
    {
        $user = $this->em->getRepository('AppBundle:User')->find(1);
        $issues = $this->em
            ->getRepository('AppBundle:Activity')
            ->getUserActivities($user);


        $this->assertTrue(!empty($issues));
    }

    public function testGetProjectActivities()
    {
        $project = $this->em->getRepository('AppBundle:Project')->find(1);
        $issues = $this->em
            ->getRepository('AppBundle:Activity')
            ->getProjectActivities($project);


        $this->assertTrue(!empty($issues));
    }

    public function testGetUserProjectsActivities()
    {
        $project = $this->em->getRepository('AppBundle:Project')->find(1);
        $issues = $this->em
            ->getRepository('AppBundle:Activity')
            ->getUserProjectsActivities([$project]);


        $this->assertTrue(!empty($issues));
    }



    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }
}
