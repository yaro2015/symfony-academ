<?php
namespace AppBundle\Tests\Functional\Form;

use AppBundle\Form\ProjectType;
use AppBundle\Entity\Project;
use Symfony\Component\Form\Test\TypeTestCase;
use Doctrine\Common\Collections\ArrayCollection;

class ProjectTypeTest extends TypeTestCase
{
    private $entityManager;

    public function setUp()
    {

        $this->marktestskipped();
        $project_type = new ProjectType();

        parent::setUp();
    }

    public function testSubmitValidData()
    {
        $users = new ArrayCollection();
        $project = new Project();

        $formData = array(
            'title' => 'title',
            'summary' => 'summary',
            'users' => $users
        );

        $form = $this->factory->create(ProjectType::class, $project);


        $object = Project::fromArray($formData);


        // submit the data to the form directly
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
        $extracted = $form->getData();

        $this->assertEquals($object, $extracted);


        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }

    }
}
