<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class DefaultControllerTest extends WebTestCase
{

    public function setUp()
    {
        $this->client = static::createClient();
        $this->logIn();
      
    }
    /**
     * @dataProvider protectedPagesProvider
     * @param $url
     */
    public function testPages($url)
    {
        $crawler = $this->client->request('GET', $url);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }
    public function protectedPagesProvider()
    {
        return [
            ['/'],
            ['/users/view/1'],
            ['/users/profile/1'],
            ['/users'],
            ['/projects'],
            ['/projects/create'],
            ['/projects/view/1'],
            ['/projects/edit/1'],
            ['/projects/1/create_issue'],
            ['/issues'],
            ['/issues/view/1'],
            ['/issues/edit/1'],
        ];
    }

    private function logIn()
    {
        $container = $this->client->getContainer();
        $user = $container->get('doctrine')->getManager()->getRepository('AppBundle:User')->find(1);
        $session = $container->get('session');
        $firewall = 'main';
        $token = new UsernamePasswordToken($user, null, $firewall, ['ROLE_ADMIN']);
        $session->set('_security_'.$firewall, serialize($token));
        $session->save();
        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}
