<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.06.16
 * Time: 14:35
 */

namespace AppBundle\Tests\Event;

use AppBundle\Entity\Issue;
use AppBundle\Event\IssueStatusChangedEvent;

class IssueStatusChangedEventTest extends \PHPUnit_Framework_TestCase
{
    public function testGetComment()
    {
        $issue = new Issue();
        $event = new IssueStatusChangedEvent($issue);
        $this->assertEquals($issue, $event->getIssue());
    }
}
