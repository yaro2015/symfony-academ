<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.06.16
 * Time: 14:35
 */

namespace AppBundle\Tests\Event;

use AppBundle\Entity\Comment;
use AppBundle\Event\IssueCommentCreatedEvent;

class IssueCommentCreatedEventTest extends \PHPUnit_Framework_TestCase
{
    public function testGetComment()
    {
        $comment = new Comment();
        $event = new IssueCommentCreatedEvent($comment);
        $this->assertEquals($comment, $event->getComment());
    }
}
