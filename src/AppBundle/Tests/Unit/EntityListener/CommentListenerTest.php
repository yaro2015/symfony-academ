<?php
/**
 * Created by Yaro in PhpStorm.
 * Date: 29/05/16
 */

namespace AppBundle\Tests\Unit\EntityListener;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Issue;
use AppBundle\Entity\User;
use AppBundle\EntityListener\CommentListener;
use AppBundle\Event\IssueCommentCreatedEvent;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Class CommentListenerTest
 * @package AppBundle\Tests\Unit\EntityListener
 */
class CommentListenerTest extends \PHPUnit_Framework_TestCase
{
    /** @var CommentListener */
    protected $listener;

    /** @var \PHPUnit_Framework_MockObject_MockObject */
    protected $eventDispatcher;

    /** @var \PHPUnit_Framework_MockObject_MockObject */
    protected $eventArgs;


    public function setUp()
    {
        $this->eventDispatcher = $this->getMockbuilder('Symfony\Component\EventDispatcher\EventDispatcherInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $this->eventArgs = $this->getMockbuilder('Doctrine\ORM\Event\LifecycleEventArgs')
            ->disableOriginalConstructor()
            ->getMock();


        $this->listener = new CommentListener($this->eventDispatcher);
    }


    public function testPostPersist()
    {
        $issue = new Issue();
        $comment = new Comment();
        $comment->setIssue($issue);
        $event = new IssueCommentCreatedEvent($comment);

        $this->eventDispatcher->expects($this->once())
            ->method('dispatch')
            ->with('issue.comment_created', $event);


        $this->listener->postPersist($comment, $this->eventArgs);

    }
}
