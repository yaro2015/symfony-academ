<?php
/**
 * Created by Yaro in PhpStorm.
 * Date: 29/05/16
 */

namespace AppBundle\Tests\Unit\EntityListener;

use AppBundle\Entity\Issue;
use AppBundle\Entity\User;
use AppBundle\EntityListener\IssueListener;
use AppBundle\Event\IssueCreatedEvent;
use AppBundle\Event\IssueStatusChangedEvent;
use AppBundle\Event\IssueUpdatedEvent;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Class CommentListenerTest
 * @package AppBundle\Tests\Unit\EntityListener
 */
class IssueListenerTest extends \PHPUnit_Framework_TestCase
{
    /** @var IssueListener  */
    protected $listener;

    /** @var \PHPUnit_Framework_MockObject_MockObject */
    protected $eventDispatcher;

    /** @var \PHPUnit_Framework_MockObject_MockObject */
    protected $eventArgs;


    public function setUp()
    {
        $this->eventDispatcher = $this->getMockbuilder('Symfony\Component\EventDispatcher\EventDispatcherInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $this->eventArgs = $this->getMockbuilder('Doctrine\ORM\Event\LifecycleEventArgs')
            ->disableOriginalConstructor()
            ->getMock();


        $this->listener = new IssueListener($this->eventDispatcher);
    }

    public function testPostPersist()
    {
        $issue = new Issue();
        $event = new IssueCreatedEvent($issue);

        $this->eventDispatcher->expects($this->once())
            ->method('dispatch')
            ->with('issue.created_event', $event);

        $this->listener->postPersist($issue, $this->eventArgs);

    }

    public function testPostUpdate()
    {
        $issue = new Issue();
        $eventUpdated = new IssueUpdatedEvent($issue);
        $eventChanged = new IssueStatusChangedEvent($issue);

        $emMock =  $this->getMockbuilder('Doctrine\ORM\EntityManager')
            ->disableOriginalConstructor()
            ->getMock();

        $unitMock = $this->getMockbuilder('Doctrine\ORM\UnitOfWork')
            ->disableOriginalConstructor()
            ->getMock();

        $this->eventArgs->expects($this->once())
            ->method('getEntityManager')
            ->willReturn($emMock);

        $emMock->expects($this->once())
            ->method('getUnitOfWork')
            ->willReturn($unitMock);

        $unitMock->expects($this->once())
            ->method('getEntityChangeSet')
            ->with($issue)
            ->willReturn(['status' => 1]);

        $this->eventDispatcher->expects($this->exactly(2))
            ->method('dispatch')
            ->withConsecutive(
                ['issue.status_changed_event', $eventChanged],
                ['issue.updated_event', $eventUpdated]
            );

        $this->listener->postUpdate($issue, $this->eventArgs);

    }
}
