<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31.05.16
 * Time: 19:05
 */

namespace AppBundle\Tests\Unit\Twig;

use AppBundle\Entity\Issue;
use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use AppBundle\Twig\ActivityExtension;
use Symfony\Component\PropertyAccess\Tests\Fixtures\TestClass;

class ActivityExtensionTest extends \PHPUnit_Framework_TestCase
{
    /** @var \PHPUnit_Framework_MockObject_MockObject */
    protected $activityRepository;

    /**
     * @var ActivityExtension
     */
    protected $extension;

    public function setUp()
    {
        $this->activityRepository = $this->getMockBuilder('AppBundle\Repository\ActivityRepository')
            ->disableOriginalConstructor()
            ->getMock();

        $this->extension = new ActivityExtension($this->activityRepository);

    }

    public function providerParams()
    {
        return [
            ['issue', 'getIssueActivities', new Issue()] ,
            ['user',  'getUserActivities', new User()],
            ['project', 'getProjectActivities', new Project()],
            ['user_projects',  'getUserProjectsActivities', [new Project()]]

        ];
    }

    /**
     * @dataProvider providerParams
     * @param $type
     * @param $expectedMethod
     */
    public function testGetActivities($type, $expectedMethod, $expectedObject)
    {
        $environment = $this->getMockBuilder('\Twig_Environment')
            ->disableOriginalConstructor()
            ->getMock();


        $template  = $this->getMockBuilder('\Twig_Template')
            ->disableOriginalConstructor()
            ->getMock();

        $template->expects($this->once())
            ->method('render');

        $environment->expects($this->once())
            ->method('loadTemplate')
            ->will($this->returnValue($template));

        $this->activityRepository->expects($this->once())
            ->method($expectedMethod)
            ->with($expectedObject);

        $this->extension->getActivities($environment, $type, $expectedObject);
    }

    public function testGetName()
    {
        $result = $this->extension->getName();
        $this->assertEquals('activity_extension', $result);
    }

    public function testGetFunctions()
    {
        $functions = $this->extension->getFunctions();
        $this->assertEquals(1, count($functions));
        $this->assertEquals('show_activities', $functions[0]->getName());
    }

    /**
     * @dataProvider providerParams
     */
    public function testGetAvailableParams($paramName)
    {
        $params = $this->extension->getAvailableParams();
        $this->assertTrue(isset($params[$paramName]));
    }
}
