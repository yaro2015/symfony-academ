<?php
namespace AppBundle\Tests\Unit\Entity;

use AppBundle\Entity\Issue;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.06.16
 * Time: 16:44
 */
class IssueTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Comment
     */
    private $entity;

    private $accessor;

    public function setUp()
    {
        $this->entity = new Issue();
        $this->accessor =  PropertyAccess::createPropertyAccessor();
    }
    public function dataFieldsProvider()
    {
        return [
            ['Created', new \DateTime()],
            ['Updated', new \DateTime()],
            ['WasClosed', 1],
            ['Summary', 'text'],
            ['Code', 'text'],
            ['Description', 'text'],
            ['Type', 'text'],
            ['Priority', 'text'],
            ['Status', 'text'],
            ['Reporter', new User()],
            ['Assignee', new User()],
            ['Children', new ArrayCollection()],
            ['Parent', new Issue()],
            ['ParentId', 1]

        ];
    }

    /**
     * @dataProvider  dataFieldsProvider
     * @param $field
     */
    public function testAccessors($field, $value)
    {
        $this->accessor->setValue($this->entity, $field, $value);
        $this->assertEquals($value, $this->accessor->getValue($this->entity, $field));
    }

    public function testCustomAccessors()
    {
        $this->assertEquals(null, $this->entity->getId());
    }
}
