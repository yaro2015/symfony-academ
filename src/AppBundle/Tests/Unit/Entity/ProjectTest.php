<?php
namespace AppBundle\Tests\Unit\Entity;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Issue;
use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.06.16
 * Time: 16:44
 */
class ProjectTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Project
     */
    private $entity;

    private $accessor;

    public function setUp()
    {
        $this->entity = new Project();
        $this->accessor =  PropertyAccess::createPropertyAccessor();
    }
    public function dataFieldsProvider()
    {
        return [
            ['Title', 'text'],
            ['Summary', 'text'],
            ['Code', 'text'],
        ];
    }

    /**
     * @dataProvider  dataFieldsProvider
     * @param $field
     */
    public function testAccessors($field, $value)
    {
        $this->accessor->setValue($this->entity, $field, $value);
        $this->assertEquals($value, $this->accessor->getValue($this->entity, $field));
    }

    public function testCustomAccessors()
    {
        $this->assertEquals(null, $this->entity->getId());
        $user = new User();
        $this->entity->addUser($user);
        $mock = $this->getMock('Symfony\Component\Validator\Context\ExecutionContextInterface');
        $mock->expects($this->never())
            ->method('buildViolation');
        $this->entity->validate($mock);
        
    }
}
