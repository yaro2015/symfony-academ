<?php
namespace AppBundle\Tests\Unit\Entity;

use AppBundle\Entity\Activity;
use AppBundle\Entity\Issue;
use AppBundle\Entity\User;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.06.16
 * Time: 16:44
 */
class ActivityTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Activity
     */
    private $entity;

    private $accessor;

    public function setUp()
    {
        $this->entity = new Activity();
        $this->accessor =  PropertyAccess::createPropertyAccessor();
    }
    public function dataFieldsProvider()
    {
        return [
            ['CreatedAt', new \DateTime()],
            ['User', new User()],
            ['UserId', 1],
            ['Issue', new Issue()],
            ['IssueId', 1],
            ['Status', 'some-string']
        ];
    }

    /**
     * @dataProvider  dataFieldsProvider
     * @param $field
     */
    public function testAccessors($field, $value)
    {
        $this->accessor->setValue($this->entity, $field, $value);
        $this->assertEquals($value, $this->accessor->getValue($this->entity, $field));
    }

    public function testGetId()
    {
        $this->assertEquals(null, $this->entity->getId());
    }
}
