<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.06.16
 * Time: 14:45
 */

namespace AppBundle\Tests\Unit\ActivityEventSubscriberTest;

use AppBundle\Entity\User;
use AppBundle\EventListener\RegistrationListener;
use FOS\UserBundle\FOSUserEvents;

class RegistrationListenerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var
     */
    private $listener;

    public function setUp()
    {
        $this->listener =  new RegistrationListener();
    }
    
    public function testGetSubscribedEvents()
    {
        $events = $this->listener->getSubscribedEvents();
        $this->assertTrue(in_array('onRegistrationSuccess', $events));
        $this->assertTrue(isset($events[FOSUserEvents::REGISTRATION_SUCCESS]));
    }

    public function testOnRegistrationSuccess()
    {
        $formEventMock = $this->getMockBuilder('FOS\UserBundle\Event\FormEvent')
                            ->disableOriginalConstructor()
                            ->getMock();

        $formMock = $this->getMockBuilder('Symfony\Component\Form\FormInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $formEventMock->expects($this->once())
            ->method('getForm')
            ->willReturn($formMock);

        $formMock->expects($this->once())
            ->method('getData')
            ->willReturn(new User())
        ;
        $this->listener->onRegistrationSuccess($formEventMock);

    }
}
