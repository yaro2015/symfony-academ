<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.06.16
 * Time: 14:45
 */

namespace AppBundle\Tests\Unit\ActivityEventSubscriberTest;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Issue;
use AppBundle\Entity\User;
use AppBundle\Event\IssueCommentCreatedEvent;
use AppBundle\Event\IssueCreatedEvent;
use AppBundle\Event\IssueStatusChangedEvent;
use AppBundle\EventListener\ActivityEventSubscriber;
use AppBundle\EventListener\IssueCollaboratorEventSubscriber;

class IssueCollaboratorEventSubscriberTest extends \PHPUnit_Framework_TestCase
{
    /**
     * EntityManager Mock
     * @var
     */
    private $em;

    /**
     * @var Issue
     */
    private $issue;

    /**
     * @var User
     */
    private $user;

    /**
     * @var IssueCollaboratorEventSubscriber
     */
    private $subscriber;

    public function setUp()
    {
        // mock entity manager
        $this->em = $this->getMockBuilder('Doctrine\ORM\EntityManager')
            ->disableOriginalConstructor()
            ->getMock();

        $this->issue = new Issue();
        $this->user = new User();
        $this->subscriber = new IssueCollaboratorEventSubscriber($this->em);
    }

    /**
     * @return array
     */
    public function eventsListProvider()
    {
        return [
            ['issue.comment_created' , 'addCommentCollaborator'],
            ['issue.created_event' , 'addIssueCollaborators'],
            ['issue.updated_event', 'addIssueCollaborators'],
        ];
    }

    /**
     * @dataProvider eventsListProvider
     */
    public function testGetSubscribedEvents($eventName, $methodName)
    {
        $events = $this->subscriber->getSubscribedEvents();
        $this->assertTrue(isset($events[$eventName]));
        $this->assertTrue(in_array($methodName, $events));
    }

    public function testAddCommentCollaborator()
    {
        $comment = new Comment();
        $comment->setIssue($this->issue);
        $comment->setUser($this->user);
        $event = new IssueCommentCreatedEvent($comment);
        $this->configureExpectations();
        $this->subscriber->addCommentCollaborator($event);
    }

    public function testAddIssueCollaborators()
    {
        $event = new IssueCreatedEvent($this->issue);
        $this->configureExpectations();
        $this->subscriber->addIssueCollaborators($event);
    }

    public function configureExpectations()
    {
        $this->issue->setAssignee($this->user);
        $this->issue->setReporter($this->user);
        $this->em->expects($this->once())
           ->method('persist')
           ->with($this->issue);
    }
}
