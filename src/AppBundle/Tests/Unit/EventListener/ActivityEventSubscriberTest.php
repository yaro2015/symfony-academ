<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.06.16
 * Time: 14:45
 */

namespace AppBundle\Tests\Unit\ActivityEventSubscriberTest;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Issue;
use AppBundle\Entity\User;
use AppBundle\Event\IssueCommentCreatedEvent;
use AppBundle\Event\IssueCreatedEvent;
use AppBundle\Event\IssueStatusChangedEvent;
use AppBundle\EventListener\ActivityEventSubscriber;

class ActivityEventSubscriberTest extends \PHPUnit_Framework_TestCase
{
    /**
     * EntityManager Mock
     * @var
     */
    private $em;

    private $tokenStorage;

    private $token;

    private $user;

    private $subscriber;

    private $mailerMock;

    private $twigMock;

    public function setUp()
    {
        // mock entity manager
        $this->em = $this->getMockBuilder('Doctrine\ORM\EntityManager')
            ->disableOriginalConstructor()
            ->getMock();


        $this->tokenStorage = $this->getMockBuilder(
            'Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface'
        )
            ->disableOriginalConstructor()
            ->getMock();

        $this->token = $this->getMockBuilder('Symfony\Component\Security\Core\Authentication\Token\TokenInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $this->mailerMock = $this->getMockBuilder('\Swift_Mailer')
            ->disableOriginalConstructor()
            ->getMock();

        $this->twigMock = $this->getMockBuilder('Symfony\Bundle\TwigBundle\TwigEngine')
            ->disableOriginalConstructor()
            ->getMock();

        $this->user = new User();

        $this->subscriber = new ActivityEventSubscriber(
            $this->em,
            $this->tokenStorage,
            $this->mailerMock,
            $this->twigMock
        );
    }

    public function eventsListProvider()
    {
        return [
            ['issue.comment_created' , 'addActivityComment'],
            ['issue.created_event' , 'addActivityIssue'],
            ['issue.status_changed_event', 'addActivityIssueStatus'],
        ];
    }

    /**
     * @dataProvider eventsListProvider
     */
    public function testGetSubscribedEvents($eventName, $methodName)
    {
        $events = $this->subscriber->getSubscribedEvents();
        $this->assertTrue(isset($events[$eventName]));
        $this->assertTrue(in_array($methodName, $events));
    }

    public function testAddActivityComment()
    {
        $comment = new Comment();
        $comment->setIssue(new Issue());
        $event = new IssueCommentCreatedEvent($comment);
        $this->configureExpectations();
        $this->subscriber->addActivityComment($event);
    }

    public function testAddActivityIssue()
    {
        $event = new IssueCreatedEvent(new Issue());
        $this->configureExpectations();
        $this->subscriber->addActivityIssue($event);
    }
    
    public function testAddActivityIssueStatus()
    {
        $event = new IssueStatusChangedEvent(new Issue());
        $this->configureExpectations();
        $this->subscriber->addActivityIssueStatus($event);
    }

    public function configureExpectations()
    {
        $this->tokenStorage->expects($this->once())
            ->method('getToken')
            ->willReturn($this->token);

        $this->token->expects($this->once())
            ->method('getUser')
            ->willReturn($this->user);

        $this->twigMock->expects($this->once())
            ->method('render')
            ->willReturn('template');

        $this->mailerMock->expects($this->once())
            ->method('send');
            
    }
}
