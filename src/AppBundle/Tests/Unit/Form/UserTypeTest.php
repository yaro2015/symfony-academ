<?php
namespace AppBundle\Tests\Unit\Form;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Component\Form\Test\TypeTestCase;
use Doctrine\Common\Persistence\ManagerRegistry;

class UserTypeTest extends TypeTestCase
{

    public function dataProvider()
    {
        return [
            [
                false,
                ['roles' => ['ROLE_ADMIN']] // Selected role
            ],
            [
                true, //options hide active
                [] //No role selected
            ]
        ];
    }

    /**
     * @dataProvider dataProvider
     * @param $hide_roles
     * @param $role
     */
    public function testSubmitValidData($hide_roles, $role)
    {

        $formData = array(
            'username' => 'admin',
            'email' => 'qwe@qwe.com',
            'fullname' => 'Foo Bar',
            'timezone' => 'Europe/Kiev'

        );
        $formData = array_merge($formData, $role);
        $options['hide_roles'] = $hide_roles;
        $object = new User();

        //First you need this is the end verify if the FormType compiles. This includes basic class inheritance,
        // the buildForm function and options resolution. This should be the first test you write:

        $form = $this->factory->create(UserType::class, $object, $options);

        // submit the data to the form directly
        $form->submit($formData);

        //This test checks that none of your data transformers used by the form failed.
        // The isSynchronized() method is only set to false if a data transformer throws an exception:

        $this->assertTrue($form->isSynchronized());

        //Next, verify the submission and mapping of the form. The test below checks
        // if all the fields are correctly specified:
        $this->assertEquals($object, $form->getData());

        //Finally, check the creation of the FormView.
        // You should check if all widgets you want to display are available in the children property:
        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }



    }
}
