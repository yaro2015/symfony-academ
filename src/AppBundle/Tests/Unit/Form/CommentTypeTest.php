<?php
namespace AppBundle\Tests\Unit\Form;

use AppBundle\Entity\Comment;
use AppBundle\Form\CommentType;
use AppBundle\Form\ProjectType;
use AppBundle\Entity\Project;
use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\DoctrineOrmExtension;
use Symfony\Bridge\Doctrine\Tests\Fixtures\User;
use Symfony\Bridge\Doctrine\Tests\Form\Type\EntityTypeTest;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Form\PreloadedExtension;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\ClassMetadata;
//use AppBundle\Service\EntityTypeFake;
use Doctrine\ORM\Configuration;

class CommentTypeTest extends TypeTestCase
{

    public function testSubmitValidData()
    {

        $formData = array(
            'body' => 'to be or not to be',
        );
        $object = new Comment();
        $object->setBody($formData['body']);
        //First you need this is the end verify if the FormType compiles. This includes basic class inheritance,
        // the buildForm function and options resolution. This should be the first test you write:

        $form = $this->factory->create(CommentType::class);

        // submit the data to the form directly
        $form->submit($formData);

        //This test checks that none of your data transformers used by the form failed.
        // The isSynchronized() method is only set to false if a data transformer throws an exception:

        $this->assertTrue($form->isSynchronized());

        //Next, verify the submission and mapping of the form. The test below checks
        // if all the fields are correctly specified:
        $this->assertEquals($object, $form->getData());

        //Finally, check the creation of the FormView.
        // You should check if all widgets you want to display are available in the children property:
        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }
}
