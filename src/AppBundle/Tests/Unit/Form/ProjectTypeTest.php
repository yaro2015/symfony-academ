<?php
namespace AppBundle\Tests\Unit\Form;

use AppBundle\Form\ProjectType;
use AppBundle\Entity\Project;
use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\DoctrineOrmExtension;
use Symfony\Bridge\Doctrine\Tests\Fixtures\User;
use Symfony\Bridge\Doctrine\Tests\Form\Type\EntityTypeTest;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Form\PreloadedExtension;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\ClassMetadata;
//use AppBundle\Service\EntityTypeFake;
use Doctrine\ORM\Configuration;

class ProjectTypeTest extends TypeTestCase
{
    /** @var AttachmentType */
    protected $projectType;

    public function setUp()
    {
        $this->projectType = new ProjectType();
    }

    public function testSetDefaultOptions()
    {
        $resolver = $this->getMock('Symfony\Component\OptionsResolver\OptionsResolver');
        $resolver->expects($this->once())
            ->method('setDefaults')
            ->with(
                [
                    'data_class' => 'AppBundle\Entity\Project'
                ]
            );
        $this->projectType->setDefaultOptions($resolver);
    }

    public function testBuildForm()
    {
        $builder = $this->getMock('Symfony\Component\Form\Test\FormBuilderInterface');

        $builder->expects($this->at(0))
            ->method('add')
            ->with('title')
            ->willReturn($builder);

        $builder->expects($this->at(1))
            ->method('add')
            ->with('summary')
            ->willReturn($builder);

        $builder->expects($this->at(2))
            ->method('add')
            ->willReturn($builder);

        $this->projectType->buildForm($builder, []);
    }
}
