<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 27.05.16
 * Time: 16:22
 */

namespace AppBundle\Tests\Security;

use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use AppBundle\Security\ProjectVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManager;

class ProjectVoterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var DesisionManager
     */
    protected $decisionManager;

    /**
     * @var TokenMock
     */
    protected $token;

    /**
     * @var ProjectVoter
     */
    protected $voter;

    /**
     * Setup configuration
     */
    protected function setUp()
    {
        $this->token = $this->getMock('Symfony\Component\Security\Core\Authentication\Token\TokenInterface');
        $this->decisionManager =  $this->getMock('Symfony\Component\Security\Core\Authorization\AccessDecisionManager');
        $this->voter = new ProjectVoter($this->decisionManager);

    }


    /**
     * @dataProvider providerVoteOnAttribute
     * @param $user
     * @param $project
     * @param $decision
     * @param $expected
     */
    public function testVoteOnAttribute($attribute, $user, $project, $decision, $expected)
    {
        $this->token->expects($this->any())
            ->method('getUser')
            ->willReturn($user);


        $this->decisionManager->expects($this->once())
            ->method('decide')
            ->with($this->token, ['ROLE_ADMIN', 'ROLE_MANAGER'])
            ->willReturn($decision);

        $result = $this->voter->voteOnAttribute($attribute, $project, $this->token);
        $this->assertEquals($expected, $result);
    }

    public function providerVoteOnAttribute()
    {
        $project = new Project();
        $user = new User();
        $project->addUser($user);
        return [
            'edit_project_opened_for_admin' => [
                'edit',
                $user,
                $project,
                true, //Admin decision true
                true
            ],
            'edit_project_closed_for_operator' => [
                'edit',
                $user,
                $project,
                false, //Admin decision false
                false
            ],
            'view_project_open_for_operator_member' => [
                'view',
                $user,
                $project,
                false, //Admin decision false
                true
            ],
        ];
    }

    /**
     * @param $attribute
     * @param $isSupported
     * @dataProvider attributeProvider
     */
    public function testSupports($attribute, $object, $isSupported)
    {
        $result = $this->voter->supports($attribute, $object);
        $this->assertEquals($isSupported, $result);
    }

    public function attributeProvider()
    {
        $project = new Project();
        $user = new User();
        return [
            ['view', $project, true],
            ['edit',  $project, true],
            ['view', $user, false],
            ['edit', $user, false],
            ['create',  $project,  false],
            ['delete',  $project, false],
        ];
    }
}
