<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 27.05.16
 * Time: 16:22
 */

namespace AppBundle\Tests\Security;

use AppBundle\Entity\Comment;
use AppBundle\Entity\User;
use AppBundle\Security\CommentVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManager;

class CommentVoterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Desision manager
     */
    protected $decisionManager;

    /**
     * @var token mock
     */
    protected $token;

    /**
     * @var CommentVoter
     */
    protected $voter;
    /**
     * Setup configuration
     */
    protected function setUp()
    {
        $this->token = $this->getMock('Symfony\Component\Security\Core\Authentication\Token\TokenInterface');
        $this->decisionManager =  $this->getMock('Symfony\Component\Security\Core\Authorization\AccessDecisionManager');
        $this->voter = new CommentVoter($this->decisionManager);
    }

    /**
     * @dataProvider providerVoteOnAttribute
     * @param $user
     * @param $project
     * @param $decision
     * @param $expected
     */
    public function testVoteOnAttribute($attribute, $user, $comment, $decision, $expected)
    {

        $this->token->expects($this->any())
            ->method('getUser')
            ->willReturn($user);


        $this->decisionManager->expects($this->once())
            ->method('decide')
            ->with($this->token, ['ROLE_ADMIN'])
            ->willReturn($decision);

        $result = $this->voter->voteOnAttribute($attribute, $comment, $this->token);
        $this->assertEquals($expected, $result);
    }

    public function providerVoteOnAttribute()
    {
        $comment = new Comment();
        $userOwner = new User();
        $userNotOwner = new User();
        $comment->setUser($userOwner);
        return [
            'modify_comment_opened_for_admin_always' => [
                'MODIFY',
                $userNotOwner,
                $comment,
                true, //Admin decision true
                true
            ],
            'modify_comment_open_creator' => [
                'MODIFY',
                $userOwner,
                $comment,
                false,
                true
            ],
            'modify_comment_closed_for_not_owner' => [
                'MODIFY',
                $userNotOwner,
                $comment,
                false,
                false
            ],

        ];
    }

    /**
     * @param $attribute
     * @param $isSupported
     * @dataProvider attributeProvider
     */
    public function testSupports($attribute, $object, $isSupported)
    {
        $result = $this->voter->supports($attribute, $object);
        $this->assertEquals($isSupported, $result);
    }

    public function attributeProvider()
    {
        $comment = new Comment();
        $user = new User();
        return [
            ['MODIFY', $comment, true],
            ['MODIFY',  $user, false],
            ['no-exist',  $user, false],

        ];
    }
}
