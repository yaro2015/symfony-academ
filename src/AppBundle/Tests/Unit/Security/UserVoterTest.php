<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 27.05.16
 * Time: 16:22
 */

namespace AppBundle\Tests\Security;

use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use AppBundle\Security\UserVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManager;

class UserVoterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Desision manager
     */
    protected $decisionManager;

    protected $token;

    protected $voter;
    /**
     * Setup configuration
     */
    protected function setUp()
    {
        $this->token = $this->getMock('Symfony\Component\Security\Core\Authentication\Token\TokenInterface');
        //$this->decisionManager =  new AccessDecisionManager();
        $this->decisionManager = $this->getMock('Symfony\Component\Security\Core\Authorization\AccessDecisionManager');
        $this->voter = new UserVoter($this->decisionManager);

    }

    /**
     * @dataProvider casesDataProvider
     * @param $user
     * @param $loggedUser
     * @param $expected
     */
    public function testVoteOnAttribute($attribute, $user, $loggedUser, $expected, $is_admin)
    {
        $this->token->expects($this->any())
            ->method('getUser')
            ->willReturn($loggedUser);


        $this->decisionManager->expects($this->any())
            ->method('decide')
            ->with($this->token, ['ROLE_ADMIN'])
            ->willReturn($is_admin);

        $result = $this->voter->voteOnAttribute($attribute, $user, $this->token);
        $this->assertEquals($expected, $result);
    }

    public function casesDataProvider()
    {
        $userProfile = new User();
        $operator = new User();
        $admin = new User();
        $admin->setRoles(['ROLE_ADMIN']);

        return [
            'user_prohibited_edit_foreign_profile' => [
                'edit',
                $userProfile,
                $operator,
                false,
                false
            ],
            'user_can_edit_own_profile' => [
                'edit',
                $operator,
                $operator,
                true,
                false
            ],
            'admin_user_can_edit_any_profile' => [
                'edit',
                $userProfile,
                $admin,
                true,
                true
            ],
            'any_user_can_view_any_profile' => [
                'view',
                $userProfile,
                $operator,
                true,
                false
            ]
        ];
    }

    /**
     * @param $attribute
     * @param $isSupported
     * @dataProvider attributeProvider
     */
    public function testSupports($attribute, $object, $isSupported)
    {

        $result = $this->voter->supports($attribute, $object);
        $this->assertEquals($isSupported, $result);
    }

    public function attributeProvider()
    {
        $project = new Project();
        $user = new User();
        return [
            ['view', $user, true],
            ['edit',  $user, true],
            ['view', $project, false],
            ['edit', $project, false],
            ['create',  $user,  false]

        ];
    }
}
