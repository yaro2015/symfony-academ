<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 27.05.16
 * Time: 12:47
 */

namespace AppBundle\EntityListener;

use AppBundle\Event\IssueCreatedEvent;
use AppBundle\Event\IssueStatusChangedEvent;
use AppBundle\Event\IssueUpdatedEvent;
use Doctrine\ORM\Event\LifecycleEventArgs;
use AppBundle\Entity\Issue;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class IssueListener
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Issue $issue
     * @param LifecycleEventArgs $eventArgs
     */
    public function postPersist(Issue $issue, LifecycleEventArgs $eventArgs)
    {
        $event = new IssueCreatedEvent($issue);
        $this->eventDispatcher->dispatch('issue.created_event', $event);
    }


    /**
     *
     * @param Issue $issue
     * @param LifecycleEventArgs $eventArgs
     *
     */
    public function postUpdate(Issue $issue, LifecycleEventArgs $eventArgs)
    {
        $changes = $eventArgs->getEntityManager()->getUnitOfWork()->getEntityChangeSet($issue);

        //we are interested only in status changes
        if (isset($changes['status'])) {
            $event = new IssueStatusChangedEvent($issue);
            $this->eventDispatcher->dispatch('issue.status_changed_event', $event);
        };
        $event = new IssueUpdatedEvent($issue);
        $this->eventDispatcher->dispatch('issue.updated_event', $event);

    }
}
