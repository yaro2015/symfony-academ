<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 27.05.16
 * Time: 12:47
 */

namespace AppBundle\EntityListener;

use AppBundle\Event\IssueCommentCreatedEvent;
use Doctrine\ORM\Event\LifecycleEventArgs;
use AppBundle\Entity\Comment;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CommentListener
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Comment $comment
     * @param LifecycleEventArgs $eventArgs
     */
    public function postPersist(Comment $comment, LifecycleEventArgs $eventArgs)
    {
        $event = new IssueCommentCreatedEvent($comment);
        $this->eventDispatcher->dispatch('issue.comment_created', $event);
    }
}
